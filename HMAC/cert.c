// -*- coding: utf-8 -*-

#include <stdio.h>
#include <openssl/sha.h>
#include <openssl/md5.h>

int main()
{
  int i, j;

  // SECRET
  unsigned char message[] = "Alain Turin";
  unsigned char secret_md5[MD5_DIGEST_LENGTH];

  MD5_CTX secret;
  MD5_Init (&secret);
  MD5_Update (&secret, message, sizeof(message) -1 );
  MD5_Final (secret_md5, &secret);

  // MESSAGE
  unsigned char resume_md5[MD5_DIGEST_LENGTH];
  unsigned char buffer[2048];
  int nb_octets_lus;

  char *nom_du_fichier="email1.txt";
  char *nom_du_fichier_auth="email1-auth.txt";
  FILE *fichier = fopen (nom_du_fichier, "rb+");
  FILE *fichier_auth = fopen (nom_du_fichier_auth, "w+");
  if (fichier == NULL) {
    printf ("Le fichier %s ne peut pas être ouvert.\n", nom_du_fichier);
    return 0;
  }
  if (fichier_auth == NULL) {
    printf ("Le fichier %s ne peut pas être ouvert.\n", nom_du_fichier_auth);
    return 0;
  }

  char c;
  do
   {
      c = fgetc(fichier);
      if( feof(fichier) )
         break ;
      if(c == '\r') {
        c = fgetc(fichier);
        if(c == '\n') {
          c = fgetc(fichier);
          if(c == '\r') {
            c = fgetc(fichier);
            if(c == '\n') {
              break;
            }
          }
        }
      }
   }while(1);

  nb_octets_lus = fread (buffer, 1, sizeof(buffer), fichier);

  for(i=0; i<2048; ++i) {
    if(buffer[i] == '\0') {
      for(j=0; j<MD5_DIGEST_LENGTH; j++) {
        buffer[i+j] = secret_md5[j];
      }
      buffer[i+MD5_DIGEST_LENGTH] = '\r';
      buffer[i+MD5_DIGEST_LENGTH+1] = '\n';
      buffer[i+MD5_DIGEST_LENGTH+2] = '\0';
      break;
    }
  }

  MD5_CTX contexte;
  MD5_Init (&contexte);
  MD5_Update (&contexte, buffer, nb_octets_lus);  


  MD5_Final (resume_md5, &contexte);

  rewind(fichier);

  nb_octets_lus = fread (buffer, 1, sizeof(buffer), fichier);

  rewind(fichier);

  for(i=0; i<nb_octets_lus; ++i) {
    if( buffer[i] == '\n' && buffer[i+1] == '\r') {
      fprintf(fichier_auth, "X-UdC_authentique: ");
      for(j = 0; j < MD5_DIGEST_LENGTH; j++)
        fprintf(fichier_auth, "%02x", resume_md5[j]);
    }
    fprintf(fichier_auth, "%c", buffer[i]);
  }

  fclose (fichier);
  fclose (fichier_auth);
  
  return 0;
}