#include <stdio.h>
#include <stdlib.h>
#include <openssl/md5.h>

#ifndef __EMAIL_H__
#define __EMAIL_H__

int get_body(FILE* f, char* body, int size);

char* get_appendice(FILE* f);

void create_file_with_appendice(FILE* f, char* appendice);

#endif
