#include <stdio.h>
#include <stdlib.h>
#include <openssl/sha.h>
#include <openssl/md5.h>
#include <string.h>

#ifndef __HMAC_H__
#define __HMAC_H__

char* string_to_hmac(char *body, int length);

#endif
