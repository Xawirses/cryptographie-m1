#include "email.h"
#include "hmac.h"

int main(int argc, char const *argv[]) {
    char *nom_email="email1.txt";
    FILE *fichier = fopen (nom_email, "rb+");
    if (fichier == NULL) {
        printf ("Le fichier %s ne peut pas être ouvert.\n", nom_email);
        return 1;
    }
    char body[2048];
    int nb_read = get_body(fichier, body, sizeof body - 1);
    body[nb_read] = '\0';

    char* h = string_to_hmac(body, nb_read);
    printf("X-UdC_authentique: %s\n", h);
    create_file_with_appendice(fichier, h);

    free(h);
    fclose(fichier);
    return 0;
}
