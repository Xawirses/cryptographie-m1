#include <stdio.h>
#include <stdlib.h>
#include <openssl/sha.h>
#include <openssl/md5.h>

#include <string.h>

unsigned char* compute_md5(unsigned char *string, int length) {
    unsigned char* sum = (unsigned char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    MD5_CTX context;
    MD5_Init(&context);
    MD5_Update(&context, string, length);
    MD5_Final(sum, &context);
    return sum;
}

char* md5_to_str(unsigned char *sum) {
    char *out = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        sprintf(&out[i * 2], "%02x", (unsigned int)sum[i]);
    return out;
}

unsigned char* compute_sha1(unsigned char *string, int length) {
    unsigned char* sum = (unsigned char*)malloc(SHA_DIGEST_LENGTH * 2 + 1);
    SHA1(string, length - 1, sum);
    return sum;
}

char* sha1_to_str(unsigned char *sum) {
    char *out = (char*)malloc(SHA_DIGEST_LENGTH * 2 + 1);
    for (int i = 0; i < SHA_DIGEST_LENGTH; ++i)
        sprintf(&out[i * 2], "%02x", (unsigned int)sum[i]);
    return out;
}

char* string_to_hmac(char *body, int length) {
    // unsigned char* test = compute_md5((unsigned char*)body, length);  // debug
    // char* test_str = md5_to_str(test);  // debug
    // printf("F(C): %s\n", test_str);  // debug
    // free(test);  // debug
    // free(test_str);  // debug
    //IPAD
    unsigned char ipad[MD5_DIGEST_LENGTH];
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        ipad[i] = 0x36;
    //OPAD
    unsigned char opad[MD5_DIGEST_LENGTH];
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        opad[i] = 0x5c;
    //SECRET
    unsigned char secret_str[] = "Alain Turin";
    unsigned char* secret = compute_md5(secret_str, sizeof secret_str - 1);
    // char* secret_str_md5 = md5_to_str(secret);  // debug
    // printf("S: %s\n", secret_str_md5);  // debug
    // free(secret_str_md5);  // debug
    // S (+) OPAD
    unsigned char s_x_opad[MD5_DIGEST_LENGTH];
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        s_x_opad[i] = secret[i] ^ opad[i];
    char* s_x_opad_str = md5_to_str(s_x_opad);
    // printf("S (+) OPAD: %s\n", s_x_opad_str);   // debug
    // S (+) IPAD
    unsigned char s_x_ipad[MD5_DIGEST_LENGTH];
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        s_x_ipad[i] = secret[i] ^ ipad[i];
    char* s_x_ipad_str = md5_to_str(s_x_ipad);
    // printf("S (+) IPAD: %s\n", s_x_ipad_str);  // debug
    // (S (+) IPAD) || C
    unsigned char* s_x_ipad_body = (unsigned char*)malloc(MD5_DIGEST_LENGTH * 2 + length + 1);
    strcat((char*)s_x_ipad_body, s_x_ipad_str);
    strcat((char*)s_x_ipad_body, body);
    // printf("(S (+) IPAD) || C: %s\n", s_x_ipad_body);  // debug
    //  F [(S (+) IPAD) || C]
    unsigned char * md5_s_x_ipad_body = compute_md5(s_x_ipad_body, MD5_DIGEST_LENGTH * 2 + length);
    char* md5_s_x_ipad_body_str = md5_to_str(md5_s_x_ipad_body);
    // printf("F [(S (+) IPAD) || C]: %s\n", md5_s_x_ipad_body_str);  // debug
    // (S (+) OPAD) || F [(S (+) IPAD) || C]
    unsigned char* s_x_opad_and_md5_s_x_ipad_body_str = (unsigned char*)malloc(MD5_DIGEST_LENGTH * 4 + 1);
    strcat((char*)s_x_opad_and_md5_s_x_ipad_body_str, s_x_opad_str);
    strcat((char*)s_x_opad_and_md5_s_x_ipad_body_str, md5_s_x_ipad_body_str);
    // printf("(S (+) OPAD) || F [(S (+) IPAD) || C]: %s\n", s_x_opad_and_md5_s_x_ipad_body_str);  // debug
    // F [(S (+) OPAD) || F [(S (+) IPAD) || C]]
    unsigned char * md5_s_x_opad_and_md5_s_x_ipad_body_str = compute_md5(s_x_opad_and_md5_s_x_ipad_body_str, MD5_DIGEST_LENGTH * 4);
    char* md5_s_x_opad_and_md5_s_x_ipad_body_str_str = md5_to_str(md5_s_x_opad_and_md5_s_x_ipad_body_str);
    // printf("F [(S (+) OPAD) || F [(S (+) IPAD) || C]]: %s\n", md5_s_x_opad_and_md5_s_x_ipad_body_str_str);  // debug

    free(s_x_opad_str);
    free(md5_s_x_ipad_body_str);
    free(md5_s_x_ipad_body);
    free(s_x_ipad_body);
    free(secret);
    free(s_x_ipad_str);
    free(s_x_opad_and_md5_s_x_ipad_body_str);
    free(md5_s_x_opad_and_md5_s_x_ipad_body_str);

    return md5_s_x_opad_and_md5_s_x_ipad_body_str_str;
}
