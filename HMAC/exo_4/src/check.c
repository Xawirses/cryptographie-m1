#include "email.h"
#include "hmac.h"

int main(int argc, char const *argv[]) {
    char *nom_email="email1-secure.txt";
    FILE *fichier = fopen (nom_email, "rb+");
    if (fichier == NULL) {
        printf ("Le fichier %s ne peut pas être ouvert.\n", nom_email);
        return 1;
    }

    char* appendice = get_appendice(fichier);
    if (strcmp(appendice, "") == 0) return 1;

    char body[2048];
    int nb_read = get_body(fichier, body, sizeof body - 1);
    body[nb_read] = '\0';

    char* appendice2 = string_to_hmac(body, nb_read);

    if (! strcmp(appendice, appendice2) == 0)
        printf("ERREUR, champ differents\n");

    printf("OK :-)\n");
    return 0;
}
