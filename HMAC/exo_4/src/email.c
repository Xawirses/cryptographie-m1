#include "email.h"

int get_body(FILE* f, char* body, int size) {
    rewind(f);
    char c;
    while((c = getc(f)) && !feof(f))
        if (c == '\r' && (c = fgetc(f)) == '\n' && (c = fgetc(f)) == '\r' && (c = fgetc(f)) =='\n')
            break;
    int nb_read = fread(body, 1, size, f);
    return nb_read;
}

char* get_appendice(FILE* f) {
    rewind(f);
    char* field = "-UdC_authentique: ";
    char c, field_present = 0;
    while((c = getc(f)) && !feof(f)) {
        if (c == 'X') {
            int i;
            for (i = 0; i < 18; ++i) {
                c = getc(f);
                if (c != field[i]) break;
            }
            if (i == 18) {
                field_present = 1;
                break;
            }
        }
    }
    if (! field_present) {
        printf("Champ non present\n");
        return "";
    }
    char* appendice = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for(int i = 0; i < MD5_DIGEST_LENGTH * 2; ++i)
        appendice[i] = getc(f);
    appendice[MD5_DIGEST_LENGTH * 2] = '\0';
    return appendice;
}

void create_file_with_appendice(FILE* f, char* appendice) {
    char *nom_secure="email1-secure.txt";
    FILE *fichier_secure = fopen (nom_secure, "w+");
    if (fichier_secure == NULL) {
        printf ("Le fichier %s ne peut pas être ouvert.\n", nom_secure);
        return;
    }
    rewind(f);
    unsigned char buffer[2048];
    int nb_read = fread(buffer, 1, sizeof(buffer), f);
    rewind(f);
    for(int i = 0; i < nb_read; ++i) {
        if(buffer[i] == '\n' && buffer[i+1] == '\r')
            fprintf(fichier_secure, "\nX-UdC_authentique: %s\r", appendice);
        fprintf(fichier_secure, "%c", buffer[i]);
    }
    fclose(fichier_secure);
}
