    #include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>       // log, pow
#include <time.h>
#include "gmp.h"

gmp_randstate_t graine;

int main (int argc, char * argv[]) {
    mpz_t x, res;
    mpz_init(x); mpz_init(res);
    int nb_premier = 0, nb_tirage = (int) pow(10, 4);
    gmp_randinit_default(graine);
    gmp_randseed_ui(graine, time(NULL));
    clock_t debut = clock();
    for (int i = 0; i < nb_tirage; i++) {
        do {
            mpz_urandomb(x, graine, 1024);
            mpz_mod_ui(res, x, 2);
        } while (mpz_sizeinbase(x, 2) != 1024 && mpz_cmp_ui(res, 0) == 0);
        if (mpz_probab_prime_p(x, 50) > 0) {
            nb_premier++;
        }
    }
    clock_t fin = clock();

    printf("Estimation de nombres premiers: %0.2f \%\n", 100 * ((float)nb_premier / (float)nb_tirage));
    printf("Estimation du nombre de tentatives moyenne: %d\n", nb_tirage / nb_premier);
    printf("Estimation du temps pour trouver un nombres premiers: %lf secondes\n", ((double)(fin - debut) / (double)CLOCKS_PER_SEC)) / (double)nb_premier;


    mpz_clear(x);
    mpz_clear(res);
    gmp_randclear(graine);
    exit(EXIT_SUCCESS);
}
