// -*- coding: utf-8 -*-

#include <stdio.h>
#include <string.h>

char *nom_du_fichier="../../HMAC/Resumes_en_C/butokuden.jpg";
char *nom_du_fichier_crypt="../../HMAC/Resumes_en_C/confidentiel.jpg";
typedef unsigned char uchar; // Les octets sont non-signés.

void echange (uchar *state, int i, int j)
{
    uchar temp = state[i]; 
    state[i] = state[j]; 
    state[j] = temp; 
}

void initialisation(uchar *state, uchar clef[])
{
  int i, j, lg;
  
  for (i=0; i < 256; i++) state[i] = i;

  lg = strlen( (char*) clef);
  j = 0;
  for (i=0; i < 256; i++) {
    j = (j + state[i] + clef[i % lg]) % 256; 
    echange(state,i,j);           // Echange des octets en i et j
  }   
}

int main() 
{
  FILE *fichier = fopen (nom_du_fichier, "rb");
  FILE *fichier_crypt = fopen (nom_du_fichier_crypt, "wb+");
  if (fichier == NULL || fichier_crypt == NULL) {
    printf ("Le fichier %s ou %s ne peut pas être ouvert.\n", nom_du_fichier, nom_du_fichier_crypt);
    return 0;
  }

  //                          k     Y     O     T     O
  uchar state[256], clef[]={0x4B, 0x59, 0x4F, 0x54, 0x4F};    
  initialisation(state, clef); 
  int i=0, j=0, w;    

  unsigned char c;
  do
   {
      c = fgetc(fichier);
      if( feof(fichier) )
         break ;

      i = (i + 1) % 256;
      j = (j + state[i]) % 256;
      echange(state,i,j);
      w = state[(state[i] + state[j]) % 256];

      unsigned char test = c ^ w;
      fprintf(fichier_crypt, "%c", test);
      
   }while(1);

  printf("\n");

  fclose (fichier);
  return 0; 
}

/*
> make
gcc -o mon_RC4 -I/usr/local/include -I/usr/include mon_RC4.c -L/usr/local/lib -L/usr/lib -lm -lssl -lcrypto -g -Wall
> ./mon_RC4
0xB2 0x39 0x63 0x05 0xF0 0x3D 0xC0 0x27 0xCC 0xC3 
> gcc -o mon_RC4 -I/usr/local/include -I/usr/include mon_RC4.c -L/usr/lib -lm -g -Wall -DLG_FLUX=20
> ./mon_RC4
0xB2 0x39 0x63 0x05 0xF0 0x3D 0xC0 0x27 0xCC 0xC3 0x52 0x4A 0x0A 0x11 0x18 0xA8 0x69 0x82 0x94 0x4F 
> 
*/
