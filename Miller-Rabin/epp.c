// -*- coding: utf-8 -*-

#include <stdlib.h>
#include <stdio.h>
#include "gmp.h"
#include <time.h>

gmp_randstate_t etat;

int est_temoin(mpz_t a, mpz_t n){
  mpz_t m, r, s, t, tmp;
  mpz_init(m); mpz_init(r); mpz_init(t); mpz_init(tmp);
  unsigned int s = 0;
  mpz_sub_ui(m, n, 1);

  mpz_mod_ui(r, m, 2);
  while(mpz_cmp_ui(r, 0) == 0){
    ++s;
    mpz_cdiv_q_ui (m, m, 2);
    mpz_mod_ui(r, m, 2);
  }
  t = m;
  // on as t et s youpiii !!!!
  // a^t - 1 mod n =/ 0
  // a^2^i *t +1 mod n =/ 0 pour i : 0 => s

  return 0;
}

int est_probablement_premier(mpz_t n)
{
  mpz_t a;
  mpz_init(a);
  for (int i=0; i<25; ++i) {
    mpz_urandomm (a, etat, n);
    gmp_printf("Test Temoin : %Zd\n", a);
    if(est_temoin(a, n)){
      mpz_clear(a);
      return 0;
    }
  }
  mpz_clear(a);
  return 1;    
}

int main()
{
  mpz_t n;               // Déclaration de l'entier GMP n
  mpz_init(n);           // Initialisation de l'entier GMP n
  mpz_set_str(n, "170141183460469231731687303715884105727", 10);
  gmp_randinit_default (etat);
  gmp_randseed_ui(etat, time(NULL));

  printf("Le nombre ");
  gmp_printf("%Zd\n", n);  // Affichage de l'entier GMP n, en décimal
  if (est_probablement_premier(n))
    printf(" est très probablement premier!\n");
  else
    printf(" n'est absolument pas premier!\n");
  
  mpz_clear(n);
  gmp_randclear(etat);
  return 0;
}


/*
> make
gcc -o epp -I/usr/local/include -I/usr/include epp.c -L/usr/local/lib -L/usr/lib -lgmp
> ./epp
Le nombre 170141183460469231731687303715884105727 est ...
>
*/
