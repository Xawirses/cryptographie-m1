// -*- coding: utf-8 -*-

import java.math.*;
import java.io.*;
import java.security.*;

import org.bouncycastle.jcajce.provider.digest.*;

public class ResumeChaine {
    public static void main(String[] args) {
	byte[] buffer, resume;
	SHA3.DigestSHA3 fonction_de_hachage;
	// Calcul de l'empreinte MD5 d'une chaîne de caractères
	String message= "Alain Turin";
	System.out.println("Message à hacher: \"" + message +"\"");
	buffer = message.getBytes();          // On récupère les octets de la chaîne
	try {
	    fonction_de_hachage = new SHA3.DigestSHA3(256);   // On charge SHA3
	    resume = fonction_de_hachage.digest(buffer);      // On calcule le résumé
	    System.out.print("Le résumé SHA3-256 de cette chaîne est: 0x"
			     + toHex(resume));
	} catch (Exception e) { e.printStackTrace(); }
    }
    public static String toHex(byte[] donnees) {
	return javax.xml.bind.DatatypeConverter.printHexBinary(donnees);
    }    
}

/* 
> javac -cp ./:./bcprov-jdk15on-153.jar ResumeChaine.java
> java -cp ./:./bcprov-jdk15on-153.jar ResumeChaine
Message à hacher: "Alain Turin"
Le résumé SHA3 de cette chaîne est: 0x09c12d5b7b8d66490d1cab36ba86a4716a3fbc8295e10e398841c890799d84e4
>
*/

