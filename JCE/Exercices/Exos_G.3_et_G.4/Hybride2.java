import java.io.*;
import java.util.Enumeration;

import java.security.cert.Certificate;
import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;

import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStore;

public class Hybride2 {
    // Pour manipuler le trousseau
    private static KeyStore magasin;
    private static final String nomDuTrousseau = "Volumineux.keystore";
    private static final char[] motDePasse = "Alain Turin".toCharArray();
    private static KeyStore.ProtectionParameter protection;
    private static final String[] paddings = {
        "AES/ECB/PKCS5Padding",
        "AES/CBC/PKCS5Padding",
        "AES/CFB/PKCS5Padding",
        "AES/OFB/PKCS5Padding",
        "AES/CTR/PKCS5Padding",
        "AES/CTS/PKCS5Padding"
    };

    private static Cipher chiffrementRSA = null;
    private static Cipher chiffrementAES = null;
    private static SecretKeySpec docClefSecrete;

    public static void main(String[] args) {
    // Le chiffrement prend des octets et renvoie des octets
        byte[] clefChiffre = null;
        byte[] clefDechiffre = null;
        byte[] docChiffre = null;
        byte[] docChiffre_iv = null;
        byte[] docDechiffre = null;
        byte[] iv = null;
        PrivateKey clefPrivee = null;
        FileInputStream fis = null;
        Enumeration<String> aliases = null;
        FileOutputStream out = null;

        // Lire clef_chiffre.pkcs1
        try {
            fis = new FileInputStream("clef_chiffree.pkcs1");
            clefChiffre = new byte[fis.available()];
            fis.read(clefChiffre);
        } catch (Exception e){
            System.out.println("Échec à lecture de la clef chiffrée: " + e);
        } finally {
            if (fis != null) try{ fis.close(); } catch (Exception e){}
        }

        //Lire doc mystère + iv
        try {
            fis = new FileInputStream("Document_mystere.pdf");
            docChiffre_iv = new byte[16];
            fis.read(docChiffre_iv);
            docChiffre = new byte[fis.available()];
            fis.read(docChiffre);
        } catch (Exception e){
            System.out.println("Échec à lecture de la clef chiffrée: " + e);
        } finally {
            if (fis != null) try{ fis.close(); } catch (Exception e){}
        }

        // Charger le trousseau dans le magasin
        try {
            magasin = KeyStore.getInstance("JKS");
            fis = new FileInputStream(nomDuTrousseau);
            magasin.load(fis, motDePasse);
        } catch (Exception e){
            System.out.println("Échec à l'ouverture du trousseau: " + e);
        } finally {
            if (fis != null) try{ fis.close(); } catch (Exception e){}
        }

        try {
            aliases = magasin.aliases();
        } catch (Exception e){
            System.out.println("Échec à lors de la récupération des alias: " + e);
        }
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            try {
                if(magasin.entryInstanceOf(alias, KeyStore.PrivateKeyEntry.class)) {
                    // Pour déchiffrer, il faut la clef privée RSA associée
                    try {
                        protection = new KeyStore.PasswordProtection(motDePasse);
                        PrivateKeyEntry entreePrivee = null;
                        entreePrivee = (KeyStore.PrivateKeyEntry) magasin.getEntry(alias,protection);
                        if (entreePrivee != null)
                            clefPrivee = entreePrivee.getPrivateKey();
                    } catch (Exception e){
                        System.out.println("Échec à lors de la lecture de la clef privée: " + e);
                    };
                    // Déchiffrer avec la clef privée RSA
                    try {
                        Cipher chiffrementRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                        chiffrementRSA.init(Cipher.DECRYPT_MODE, clefPrivee);
                        clefDechiffre = chiffrementRSA.doFinal(clefChiffre);
                        System.out.println("clefDechiffree: " + toHex(clefDechiffre));
                    } catch (Exception e){
                        System.out.println("Échec déchiffrement.");
                    };
                    // Si clé AES
                    if (clefDechiffre.length == 16 || clefDechiffre.length == 24 || clefDechiffre.length == 32) {
                        System.out.println("\ttaille OK.");
                        // On déchiffre de l'AES
                        int i = 0;
                        for (String padding : paddings) {
                            System.out.println("\t" + padding);
                            // Récupérer un objet qui déchiffre en AES
                            try {
                                chiffrementAES = Cipher.getInstance(padding);
                            } catch (Exception e) { System.out.println("padding non disponible:" + padding);}
                            // Fabriquer la clé AES de 128 bits correspondante
                            System.out.println("\tdocClefSecrete: " + toHex(clefDechiffre));
                            docClefSecrete = new SecretKeySpec(clefDechiffre, "AES");
                            // On a tout, on dechiffre
                            try {
                                if(padding == "AES/ECB/PKCS5Padding") {
                                    chiffrementAES.init(Cipher.DECRYPT_MODE, docClefSecrete);
                                } else {
                                    // préparer le vecteur d'initialisation.
                                    IvParameterSpec ivspec = new IvParameterSpec(docChiffre_iv);
                                    chiffrementAES.init(Cipher.DECRYPT_MODE, docClefSecrete, ivspec);
                                }
                                docDechiffre = chiffrementAES.doFinal(docChiffre);
                                out = new FileOutputStream("Document_mystere_dechiffrement_" + (++i) + ".pdf");
                                byte[] document = new String(docDechiffre).getBytes();
                                out.write(document);
                                out.close();
                                System.out.println("\tEcriture dans : Document_mystere_dechiffrement_" + i + ".pdf");
                            } catch (Exception e) { System.out.println("\t\tÉchec Déchiffrement");}
                        }
                    }
                }
            } catch (Exception e){
                // System.out.println("Clef non privée");
            }
        }
    }

    public static String toHex(byte[] donnees) {
        return "0x"+javax.xml.bind.DatatypeConverter.printHexBinary(donnees);
    }
}
