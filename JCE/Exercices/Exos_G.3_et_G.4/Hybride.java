// -*- coding: utf-8 -*-

import java.io.*;
import java.util.Enumeration;

import java.security.cert.Certificate;
import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;

import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStore;

public class Hybride {
    // Pour manipuler le trousseau
    private static KeyStore magasin;
    private static final String nomDuTrousseau = "Volumineux.keystore";
    private static final char[] motDePasse = "Alain Turin".toCharArray();
    private static KeyStore.ProtectionParameter protection;
    private static String alias;

    private static Cipher chiffrementRSA = null;
    private static Cipher chiffrementAES = null;

    private static final String[] paddingAES = {"AES/ECB/PKCS5Padding", "AES/CBC/PKCS5Padding", "AES/CFB/PKCS5Padding", "AES/OFB/PKCS5Padding", "AES/CTR/PKCS5Padding", "AES/CTS/PKCS5Padding"};

    public static void main(String[] args) {
        // FileInputStream clefS = null;
        // byte[] buffer = null;
        // try {
        //     clefS = new FileInputStream("clef_chiffree.pkcs1");
        //     buffer = new byte[clefS.available()];
        //     int nbOcte = clefS.read(buffer);
        // } catch (Exception e) {
        //     System.out.println("Échec à l'ouverture du trousseau: " + e);
        // } finally {
        //     if (clefS != null) try{ clefS.close(); } catch (Exception e){}
        // }

        // FileInputStream docMyst = null;
        // FileInputStream docMystSpec = null;
        // byte[] bufferDocMyst = null;
        // byte[] bufferIV = new byte[16];
        // try {
        //     docMyst = new FileInputStream("Document_mystere.pdf");
        //     bufferDocMyst = new byte[docMyst.available()];
        //     int nbOcte = docMyst.read(bufferDocMyst);

        //     for (int i=0; i<16; i++) {
        //         bufferIV[i] = bufferDocMyst[i];
        //     }

        // } catch (Exception e) {
        //     System.out.println("Échec à l'ouverture du doc: " + e);
        // } finally {
        //     if (docMyst != null) try{ docMyst.close(); } catch (Exception e){}
        // }

        //------------------------------------------------------------------
        //  Etape 1.   Charger le trousseau dans le magasinalias
        //------------------------------------------------------------------
        FileInputStream fis = null;
        try {
            magasin = KeyStore.getInstance("JKS");
            fis = new FileInputStream(nomDuTrousseau);
            magasin.load(fis, motDePasse);
            protection = new KeyStore.PasswordProtection(motDePasse);
        } catch (Exception e) {
            System.out.println("Échec à l'ouverture du trousseau: " + e);
        } finally {
            if (fis != null) try{ fis.close(); } catch (Exception e){}
        }

        try {
            Enumeration<String> aliases = magasin.aliases();
            chiffrementRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                if(magasin.entryInstanceOf(alias, KeyStore.PrivateKeyEntry.class)) {
                    PrivateKeyEntry entreePrivee = (KeyStore.PrivateKeyEntry) magasin.getEntry(alias, protection);
                    PrivateKey clefPrivee = entreePrivee.getPrivateKey();
                    try {
                        chiffrementRSA.init(Cipher.DECRYPT_MODE, clefPrivee);
                        byte[] appendice = chiffrementRSA.doFinal(buffer);
                        if(appendice.length == 16 || appendice.length == 24 || appendice.length == 32) {
                            System.out.println(alias);
                            System.out.println(toHex(appendice));

                            int i = 0;
                            for (String padding : paddingAES) {
                                SecretKeySpec privKeySpec = new SecretKeySpec(appendice, "AES");
                                chiffrementAES = Cipher.getInstance(padding);
                                if(padding == "AES/ECB/PKCS5Padding") {
                                    chiffrementAES.init(Cipher.DECRYPT_MODE, privKeySpec);
                                } else {
                                    IvParameterSpec ivspec = new IvParameterSpec(bufferIV);
                                    chiffrementAES.init(Cipher.DECRYPT_MODE, privKeySpec, ivspec);
                                }

                                /* TODO */
                                FileOutputStream out = new FileOutputStream("file_"+i+".pdf");

                                FileInputStream docM = new FileInputStream("Document_mystere.pdf");
                                CipherInputStream cis = new CipherInputStream(docM, chiffrementAES);

                                byte[] cifBuffer = null;

                                int len = cis.read(cifBuffer);
                                out.write(cifBuffer, 0, len);

                                out.close();
                                cis.close();
                                /* END TODO */
                                i++;
                            }

                        }
                    } catch (Exception e) {}
                }
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }


    }

    public static String toHex(byte[] donnees) {
    return "0x"+javax.xml.bind.DatatypeConverter.printHexBinary(donnees);
    }
}
