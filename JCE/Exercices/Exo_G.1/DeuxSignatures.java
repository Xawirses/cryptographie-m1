// -*- coding: utf-8 -*-

import java.io.*;
import java.security.*;
import javax.crypto.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.security.KeyFactory;

import java.math.BigInteger;

public class DeuxSignatures {
    public static void main(String[] args) throws Exception {
	
	/* La clef privee (n,d) pour signer ou déchiffrer */
	BigInteger moduleN = new BigInteger("1115C8E765AE871003DDD34065AC60327"+
					    "C1A50827BBB0D0B06E5AD36F3B87AA7BB"+
					    "271D13500DEC701E253E48DC123D37A5A"+
					    "8113C343C59745DDF43CA345B7E5B1A89"+
					    "BB2773694F05F43879C58FFD35919769E"+
					    "FD49DB4E78D39D18D3FD66993BD4F27E0"+
					    "45E9E3C8FB6938CA64786172EA5832096"+
					    "8DA4B397B5BFB8B2673E1CE91EADA9E93", 16);
	System.out.println("Module de la clef: 0x" + moduleN.toString(16));	
	BigInteger exposantD = new BigInteger("9b682f2ca6f44cb60493288a686de5d81"+
					      "eca6b6d", 16);	
	System.out.println("Exposant privé de la clef: 0x" + exposantD.toString(16));

	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    RSAPrivateKeySpec privKeySpec = new RSAPrivateKeySpec(moduleN, exposantD);

    RSAPrivateKey privKey = (RSAPrivateKey) keyFactory.generatePrivate(privKeySpec);
²
	Signature signeur = Signature.getInstance("MD5withRSA");
	PrivateKey clefPrivee = (PrivateKey) privKey;

	/* Calcul du résumé MD5 du fichier "Releve_non_signe.pdf" */

	FileInputStream fis = new FileInputStream("Releve_non_signe.pdf");
	byte[] buffer = new byte[fis.available()];
	int nbOcte = fis.read(buffer);
	fis.close();

	MessageDigest fct = MessageDigest.getInstance("MD5");
	byte[] resume = fct.digest(buffer);

	// **
	    
	System.out.println("Le résumé MD5 du fichier \"Releve_non_signe.pdf\" vaut: 0x"+
			 toHex(resume));
		
	/* Déchiffrement de ce résumé MD5 avec la clef privée */
	
	Cipher chiff = Cipher.getInstance("RSA/ECB/NOPADDING");
	chiff.init(Cipher.DECRYPT_MODE, clefPrivee);

	byte [] appendice = chiff.doFinal(resume);

	// **

	System.out.println("Appendice du fichier \"Releve_non_signe.pdf\" : Ox"
			   + toHex(appendice));
	System.out.println("C'est bien la signature indiquée dans \"Releve_signe.pdf\"");

	/* Calcul de l'appendice avec un objet de la classe Signature */

	signeur.initSign(clefPrivee);
	signeur.update(buffer, 0, nbOcte);

	byte [] appendiceD = signeur.sign();

	// **

	System.out.println("Appendice (direct) du fichier \"Releve_non_signe.pdf\" : Ox"
			   + toHex(appendiceD));
	System.out.println("C'est différent: il faut lire le PKCS #1 v2.2 pour savoir pourquoi.");
      }
    
    public static String toHex(byte[] donnees) {
	return javax.xml.bind.DatatypeConverter.printHexBinary(donnees);
    }
}

/* 

> javac DeuxSignatures.java
> java DeuxSignatures
Module de la clef: 0x1115c8e765ae871003ddd34065ac60327c1a50827bbb0d0b06e5ad36f3b87aa7bb271d13500dec701e253e48dc123d37a5a8113c343c59745ddf43ca345b7e5b1a89bb2773694f05f43879c58ffd35919769efd49db4e78d39d18d3fd66993bd4f27e045e9e3c8fb6938ca64786172ea58320968da4b397b5bfb8b2673e1ce91eada9e93
Exposant privé de la clef: 0x9b682f2ca6f44cb60493288a686de5d81eca6b6d
Le résumé MD5 du fichier "Releve_non_signe.pdf" vaut: 0xCCB8A21BB6DDCDB1680A3653D28DF9F5
Appendice (indirect) du fichier "Releve_non_signe.pdf" : Ox06BED56462189266319ED6B38114EFC6326C912BF59EE5A6AC64BEC4207C07B07B25EE42A2F245A2DCF3881F8B78DE40D1B033D66FEF128B430E0E68FDBC121AF80EACE24426B8EC50E96E0843C6EAAE5B43D929DEFB8994F1DA9274434D8FEC0FCE9D5DFAA9E2393B4C95378771CFEC4B7A560B6EA3CE69869977150F91892B53E209E7
C'est bien la signature indiquée dans "Releve_signe.pdf"
Appendice (direct) du fichier "Releve_non_signe.pdf" : Ox05C866E0E24760C22C3EBAEAC4F88E0618756522A6B0E554B2085D73DCFD791ED27AADD157106CBB9893490E147B2CB488B298977D7E0F84884553AD599804CCAE49A7493A84037798C6F64A7630740049038FFD1E3A02297038265D89E77A44C12F4AD472C1023732A73991E1828498F6635D6F5EEFB39220081908D2957E6651BB0A31
C'est différent: il faut lire le PKCS #1 v2.2 pour savoir pourquoi.
> 

*/

